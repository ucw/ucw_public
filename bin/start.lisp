;; -*- lisp -*-

(in-package :common-lisp-user)

#+cmu
(defun init-cmu-mp ()
  ;; this isn't strictly necessary, but scheduling feels very coarse
  ;; without startup-idle-and-top-level-loops, leading to answer delays
  ;; of about 1s per request.
  (unless (find-if
           #'(lambda (proc) (string= (mp:process-name proc) "Top Level Loop"))
           (mp:all-processes))
    (mp::startup-idle-and-top-level-loops)))

#+cmu
(init-cmu-mp)

;;;; * UCW server initialization "script" 

;;;; This file is meant to be loaded by ucwctl, but you can use it a
;;;; general "startup ucw" file as well. You should customize this
;;;; script to load/prepare your application.

;;;; ** Loadup dependencies

;;;; Load arnesi first so we can set arnesi::*call/cc-returns* before
;;;; ucw is compiled and loaded.
(asdf:oos 'asdf:load-op :arnesi)
(setf arnesi::*call/cc-returns* nil)

;;;; Load up UCW itself
(asdf:oos 'asdf:load-op :ucw)

(in-package :it.bese.ucw-user)

#+(and sbcl sb-unicode)
(setf (external-format-for :slime) :utf-8-unix
      (external-format-for :url)   :utf-8
      (external-format-for :http-emacsen)  :utf-8-unix
      (external-format-for :http-lispish)  :utf-8)

;;;; Load the default applications systems

(asdf:oos 'asdf:load-op :ucw.examples)
(asdf:oos 'asdf:load-op :ucw.admin)

;;;; Let there be swank.
(ucw:start-swank)

;;;; Finally startup the server

(ucw:create-server :backend '(:httpd    :host "127.0.0.1" :port 8080)
                         ;; '(:mod-lisp :host "127.0.0.1" :port 3001)
                         ;; '(:araneida :host "127.0.0.1" :port 8080)
                         ;; '(:aserve   :host "127.0.0.1" :port 8080)
                   :applications (list it.bese.ucw-user::*example-application*
                                       ucw::*admin-application*)
                   :inspect-components nil
                   :log-root-directory (make-pathname :name nil :type nil
                                                      :directory (append (pathname-directory *load-truename*)
                                                                         (list :up "logs"))
                                                      :defaults *load-truename*)
                   :log-level +info+
                   :start-p t)
