;;;; -*- lisp -*-

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; This script creates, on openmcl, clisp and sbcl, a lisp image
;;;; which already contains UCW and the related libs. This greatly
;;;; reduces startup times and simplifies distribution.
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;; First we load up ucw itself

(in-package :common-lisp-user)

(asdf:oos 'asdf:load-op :arnesi)

(eval-when (:compile-toplevel :load-toplevel :execute)
  (setf arnesi::*call/cc-returns* nil))

(asdf:oos 'asdf:load-op :ucw)

(in-package :it.bese.ucw-user)

;;;; this *ucw-tal-root* isn't actually used by the admin and the
;;;; example apps (due to the fix-directories calls in ucw-toplevel).
(setf *ucw-tal-root* (merge-pathnames "../wwwroot/" *load-truename*))

;;;; Chose which backend we want to use in our image.

(setf *default-server* (make-instance 'ucw:standard-server))

(asdf:oos 'asdf:load-op :ucw.mod-lisp)
(setf (server.backend *default-server*) (make-instance 'ucw:mod-lisp-backend :port 3001))

;;; aserve 
;(asdf:oos 'asdf:load-op :ucw.aserve)
;(setf (server.backend *default-server*) (make-instance 'ucw:aserve-backend :port 8000))

;;; araneida
;(asdf:oos 'asdf:load-op :ucw.araneida)
;(setf (server.backend *default-server*) (make-instance 'ucw:araneida-backend))

;;;; We'll include the admin and example applications

(asdf:oos 'asdf:load-op :ucw.examples)
(asdf:oos 'asdf:load-op :ucw.admin)

(setf (debug-on-error nil) t
      *inspect-components* nil)

(defvar *ucw-install-root* *default-pathname-defaults*
  "The location where, on the production server, UCW is installed.

This variable is used to setup the initial wwwroot and tal
roots.")

(defun ucw-toplevel ()
  (setf *ucw-tal-root* (merge-pathnames "./wwwroot/" *ucw-install-root*))

  ;; setup the loggers  
  (let* ((ucw-logger (get-logger 'ucw::ucw-logger))
         (ucw.backend (get-logger 'ucw::ucw.backend))
         (ucw.log (merge-pathnames "./logs/ucw.log" *ucw-install-root*))
         (ucw.backend.log (merge-pathnames "./logs/ucw-backend.log" *ucw-install-root*))
         (console-appender (make-instance 'brief-stream-log-appender
                                          :stream *debug-io*)))
    (setf (appenders ucw-logger) (list console-appender (make-file-log-appender ucw.log))
          (log.level ucw-logger) +info+
          (appenders ucw.backend) (list console-appender (make-file-log-appender ucw.backend.log))))

  ;; the application objects are created when building the image, the
  ;; problem is that the pathnames created when building the image may
  ;; be different than those used when runnig the image, even if its
  ;; the same lisp on the same machine.
  (labels ((fix-generator-directory (app dir)
             (setf (yaclml::root-directories (ucw::application.tal-generator app))
                   (list dir)))
           (fix-wwwroot (app dir)
             (setf (ucw::application.www-roots app) (list dir))))
    (fix-generator-directory *example-application* (merge-pathnames "./wwwroot/" *ucw-install-root*))
    (fix-wwwroot *example-application* (merge-pathnames "./wwwroot/ucw/examples/" *ucw-install-root*))
    (fix-generator-directory ucw::*admin-application* (merge-pathnames "./wwwroot/" *ucw-install-root*))
    (fix-wwwroot ucw::*admin-application* (merge-pathnames "./wwwroot/ucw/admin/" *ucw-install-root*)))

  ;; register the applications
  (register-application *default-server* ucw::*admin-application*)
  (register-application *default-server* *example-application*)

  ;; finally we get the toplevel loop going. when possible
  ;; (multithread lisps) this starts ucw, slime and a repl, otherwise
  ;; we just start ucw

  (ucw:startup-server ucw:*default-server*)

  ;; if we get here then we're on a multithread lisp.

  (setf *package* (find-package :it.bese.ucw-user))
  
  ;; start slime
  (swank:create-server :dont-close t)
  ;; start a repl
  #+openmcl (ccl::toplevel-loop)
  #+sbcl (sb-impl::toplevel-repl)
  #+lispworks(system::listener-top-loop)
  (error "The function ucw-toplevel sholud never return."))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;; Save the image

#+openmcl
(ccl::save-application "ucw.image" :toplevel-function #'ucw-toplevel :purify t)

#+clisp
(ext:saveinitmem "ucw.mem"
                 :init-function #'ucw-toplevel
                 :start-package (find-package :it.bese.ucw-user))

#+clisp
(ext:quit)

#+sbcl
(sb-ext:save-lisp-and-die "ucw.core" :toplevel #'ucw-toplevel :purify t)

#+lispworks
(cl-user::save-image "ucw.image" :environment nil :multiprocessing t :restart-function #'ucw-toplevel)
