;; -*- lisp -*-

(eval-when (:compile-toplevel :load-toplevel :execute)
  (unless (find-package :it.bese.ucw.system)
    (defpackage :it.bese.ucw.system
      (:use :common-lisp :asdf))))

(in-package :it.bese.ucw.system)

(defsystem :UCW
  :description "UnCommon Web - A Common Lisp Web Application Framework."
  :long-description "A continuation based, component oriented
dynamic web application framework written in Common Lisp."
  :author "Marco Baringer <mb@bese.it>"
  :licence "BSD (sans advertising clause)"
  :version "0.3.9"
  :components
  ((:module :src
    :components ((:file "packages")
                 (:file "helpers" :depends-on ("packages"))
                 (:file "loggers" :depends-on ("packages"))
                 (:file "vars" :depends-on ("packages"))
                 (:module :backend
                  :components ((:file "accept"))
                  :depends-on ("packages" "loggers" :rerl))
                 (:module :rerl
                  :components ((:file "conditions" :depends-on ("protocol"))
                               (:file "cookie-session" :depends-on ("protocol"
                                                                    "standard-application"))
                               (:file "backtracking" :depends-on ("standard-classes"))
                               (:file "protocol")
                               (:file "request-loop-error" :depends-on ("conditions"))
                               (:file "standard-classes" :depends-on ("protocol"
                                                                      "standard-vars"))
                               (:file "standard-action" :depends-on ("protocol"
                                                                     "standard-classes"))
                               (:file "standard-application" :depends-on ("protocol"
                                                                          "standard-classes"
                                                                          "standard-vars"))
                               (:module :standard-component
                                        :components ((:file "standard-component" :depends-on ("standard-component-class"))
                                                     (:file "control-flow" :depends-on ("standard-component"))
                                                     (:file "standard-component-class")
                                                     (:file "transactions" :depends-on ("standard-component")))
                                        :depends-on ("backtracking"
                                                     "protocol"
                                                     "standard-action"
                                                     "standard-classes"
                                                     "standard-vars"))                               
                               (:file "standard-request-context" :depends-on ("protocol"
                                                                              "standard-classes"
                                                                              "standard-vars"
                                                                              :standard-component))
                               (:file "standard-server" :depends-on ("protocol"
                                                                     "request-loop-error"
                                                                     "standard-classes"))
                               (:file "standard-session" :depends-on ("protocol"
                                                                      "standard-classes"
                                                                      "standard-session-frame"
                                                                      "standard-vars"))
                               (:file "standard-session-frame" :depends-on ("protocol"
                                                                            "backtracking"
                                                                            "standard-classes"
                                                                            "standard-vars"
                                                                            "request-loop-error"))
                               (:file "standard-vars"))
                  :depends-on ("packages" "loggers" "helpers" "vars"))
                 (:module :components
                  :components ((:file "secure-application")
                               (:file "user-login" :depends-on ("form"
                                                                "status-bar"
                                                                "window"))
                               (:file "container")
                               (:file "cached")
                               (:file "error" :depends-on ("ucw-inspector" "window"))
                               (:file "form")
                               (:file "login")
                               (:file "message")
                               (:file "task")
                               (:file "option-dialog" :depends-on ("template"))
                               (:file "range-view" :depends-on ("template"))
                               (:file "redirect")
                               (:file "status-bar")
                               (:file "tabbed-pane" :depends-on ("container" "template"))
                               (:file "template")
                               (:file "transaction-mixin")
                               (:file "ucw-inspector")
                               (:file "widget")
                               (:file "window"))
                  :depends-on (:rerl "packages" "loggers" :html))
                 (:module :html
                  :components ((:module :yaclml
                                :components ((:file "tal")
                                             (:file "ucw-tags")
                                             (:file "yaclml"))))
                  :depends-on ("packages" "loggers" :rerl))
                 (:module :i18n
                  :components ((:file "i18n"))
                  :depends-on ("packages" :rerl))
		 (:file "control"
		  :depends-on ("packages" :backend :rerl)))))
  :properties ((version "0.3.9"))
  :depends-on (:arnesi :yaclml :swank :iterate :parenscript :cl-ppcre
               :trivial-sockets :rfc2109))

;; UCW applications included in ucw itself

(defsystem :ucw.admin
    :components ((:module :src
                  :pathname "src/admin/"
                  :components ((:file "admin")
                               (:file "admin-inspector"))))
    :depends-on (:ucw))

(defsystem :ucw.examples
    :components ((:module :examples
		  :components ((:file "examples")
			       (:file "counter" :depends-on ("examples"))
                               (:file "cache" :depends-on ("examples"))
                               (:file "forms" :depends-on ("examples"))
			       (:file "sum" :depends-on ("examples")))))
    :depends-on (:ucw))

;; Backends

(defsystem :ucw.httpd
  :components ((:module :src
                :pathname "src/backend/"
                :components ((:file "common")
                             (:file "httpd" :depends-on ("common"))
                             (:file "multithread-httpd" :depends-on ("httpd")))))
  :depends-on (:ucw :rfc2388 :puri :split-sequence))

(defsystem :ucw.mod-lisp
  :components ((:module :src
                :pathname "src/backend/"
                :components ((:file "mod-lisp"))))
  :depends-on (:ucw :ucw.httpd))

(defsystem :ucw.aserve
  :components ((:module :src
                :pathname "src/backend/"
                :components ((:file "aserve" :depends-on ("aserve-locator"))
                             (:file "aserve-locator"))))
  :depends-on (:ucw :aserve :cl-ppcre))

(defsystem :ucw.araneida
  :components ((:module :src
                :pathname "src/backend/"
                :components ((:file "common")
                             (:file "araneida" :depends-on ("common")))))
  :depends-on (:ucw :araneida :rfc2388))

;; The i18n subsystem has not been tested in a long time, use with
;; caution.

(defsystem :ucw.cl-icu
  :components ((:module :src
                :components ((:module :i18n
                              :components ((:file "cl-icu"))))))
  :depends-on (:ucw :cl-icu))

(defsystem :ucw.examples.i18n
    :components ((:module :examples
		  :components ((:file "i18n"))))
    :depends-on (:ucw :ucw.cl-icu :ucw.examples))

;; This ensures that we're loading the right versions of arnesi and
;; yaclml (add similar code for rfc2388, mod_lisp, aserve et al. when
;; they have them).

(defun ensure-system-has-feature
    (system-name version-string &optional (hint ""))
  (let* ((features (asdf:component-property (asdf:find-system system-name) :features))
         (message (format nil "UCW requires the ~A feature of system ~S.
~S currently only provides ~S.~%~A"
                          version-string system-name system-name features hint)))
    (unless (member version-string features :test #'string-equal)
      (error message))))

(defmethod asdf:perform :after ((op t) (system (eql (asdf:find-system :ucw))))
  (ensure-system-has-feature :arnesi "cc-interpreter"
                             "Try pull'ing the latest arnesi or send an email to bese-devel@common-lisp.net")
  (ensure-system-has-feature :yaclml "v0.5.2"
                             "Try pull'ing the latest yaclml or send an email to bese-devel@common-lisp.net"))

;;;; * Introduction

;;;; This is the source code to UnCommon Web (aka UCW), "the UnCommon
;;;; Web Application Framework"

;;;;@include "src/packages.lisp"

;;;;@include "src/loggers.lisp"

;;;;@include "src/helpers.lisp"

;;;;@include "src/vars.lisp"
