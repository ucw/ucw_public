;; -*- lisp -*-

(in-package :it.bese.ucw-user)

(defcomponent cache-example ()
  ())

(defmethod render ((c cache-example))
  (<:p "Last effective rendering at: " (<:as-html (get-universal-time))))

(defcomponent timeout-cache-example (cache-example timeout-cache-component)
  ()
  (:default-initargs :timeout 10))

(defcomponent hits-cache-example (cache-example num-hits-cache-component)
  ()
  (:default-initargs :timeout 5))


