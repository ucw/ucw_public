;; -*- lisp -*-

(in-package :it.bese.ucw)

;;;; *** Cookie session

(defclass cookie-session-application (standard-application)
  ()
  (:documentation "Class for applications which use cookies for sesion tracking.

Cookie session applications work exactly like
standard-applications except that when the session is not found
using the standard mechanisms the id is looked for in a cookie."))

(defclass cookie-session-request-context (standard-request-context)
  ())

(defmethod make-request-context ((app cookie-session-application)
                                 (request request)
                                 (response response))
  (make-instance 'cookie-session-request-context
                 :request request
                 :response response
                 :application app))

(defvar +ucw-session-name+ "ucw-session-id"
  "Name of the cookie used when storing the session id.")

(defmethod service :after ((app cookie-session-application) (context cookie-session-request-context))
  "Add a Set-Cookie for the session-id to the response.

The cookie is named +ucw-session-name+ and its value is the id of
context's session."
  (add-header (context.response context) "Set-Cookie"
              (rfc2109:cookie-string-from-cookie-struct
               (rfc2109:make-cookie :name +ucw-session-name+
                                    :value (session.id (context.session context))))))
 
(defmethod find-session-id ((context cookie-session-request-context))
  (or (call-next-method)
      (loop
         for cookie in (rfc2109:parse-cookies
                        (get-header (context.request context) "Cookie"))
         if (string= (rfc2109:cookie-name cookie) +ucw-session-name+)
           return (rfc2109:cookie-value cookie)
         finally (return nil))))

;; Copyright (c) 2003-2005 Edward Marco Baringer
;; All rights reserved. 
;; 
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions are
;; met:
;; 
;;  - Redistributions of source code must retain the above copyright
;;    notice, this list of conditions and the following disclaimer.
;; 
;;  - Redistributions in binary form must reproduce the above copyright
;;    notice, this list of conditions and the following disclaimer in the
;;    documentation and/or other materials provided with the distribution.
;; 
;;  - Neither the name of Edward Marco Baringer, nor BESE, nor the names
;;    of its contributors may be used to endorse or promote products
;;    derived from this software without specific prior written permission.
;; 
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
;; A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT
;; OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;; SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
;; LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
;; THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
;; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
;; OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
