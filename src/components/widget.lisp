;; -*- lisp -*-

(in-package :it.bese.ucw)

;;;; ** Widget

(defclass generic-widget-component (component)
  ((css-class :accessor widget-component.css-class
              :initform nil
              :initarg :css-class
              :type (or null string list))
   (css-id :accessor widget-component.css-id
           :initform nil
           :initarg :css-id
           :type (or null string))
   (css-style :accessor widget-component.css-style
              :initform nil
              :initarg :css-style
              :type (or null string)))
  (:documentation "A web widget.

Widget components manage the state and the graphical presentation
for one, generally small, element of the user interface.  Widgets
are not standard components, they are not CALL'able and never
ANSWER, they are simply wrapper for graphical elements (like form
inputs).

If the css-class, css-id or css-style slots are true then the
component will be wrapped (when rendering) in a <div> tag with
the corresponding attributes set."))

(defclass widget-component (generic-widget-component)
  ()
  (:documentation "A widget which should be wrapped in a <div>."))

(defclass inline-widget-component (generic-widget-component)
  ()
  (:documentation "A widget which should be wrapped in <span> and not <div>"))

(defmacro widget-render-helper (widget div-or-span)
  `(with-slots (css-class css-id css-style)
       ,widget
     (if (or css-class css-id css-style)
         (,div-or-span :class css-class
                       :id css-id
                       :style css-style
           (call-next-method))
        (call-next-method))))

(defmethod render :wrapping ((widget widget-component))
  "Wrap WIDGET in a <div> tag."
  (widget-render-helper widget <:div))

(defmethod render :wrapping ((widget inline-widget-component))
  "Wrap widget in a <span> tag."
  (widget-render-helper widget <:span))

;; Copyright (c) 2003-2005 Edward Marco Baringer
;; All rights reserved. 
;; 
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions are
;; met:
;; 
;;  - Redistributions of source code must retain the above copyright
;;    notice, this list of conditions and the following disclaimer.
;; 
;;  - Redistributions in binary form must reproduce the above copyright
;;    notice, this list of conditions and the following disclaimer in the
;;    documentation and/or other materials provided with the distribution.
;; 
;;  - Neither the name of Edward Marco Baringer, nor BESE, nor the names
;;    of its contributors may be used to endorse or promote products
;;    derived from this software without specific prior written permission.
;; 
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
;; A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT
;; OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;; SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
;; LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
;; THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
;; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
;; OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
