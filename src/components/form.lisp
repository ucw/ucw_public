;; -*- lisp -*-

(in-package :it.bese.ucw)

;;;; * UCW Web Form Library

;;;; ** FORM-FIELDS

;;;; Every input (or a set of inputs which correspond to one "value")
;;;; are represented by a form-field instance.

(defclass form-field (widget-component)
  ((validators :accessor validators :initform '() :initarg :validators
               :documentation "List of validators which will be
               applied to this field.")
   (initially-validate :accessor initially-validate :initform t
                       :documentation "When non-NIL the
                       validotars will be run as soon as the page
                       is rendered.")))

(defmethod shared-initialize :after ((field form-field)
                                     slot-names
                                     &key (value nil value-p) &allow-other-keys)
  (declare (ignore slot-names))
  (when value-p
    (setf (value field) value)))

(defgeneric form-field-p (object)
  (:method ((object form-field)) t)
  (:method ((object t)) nil))

(defmethod print-object ((field form-field) stream)
  (print-unreadable-object (field stream :type t :identity t)
    (format stream "~S" (value field))))

(defclass generic-html-input (form-field)
  ((dom-id :accessor dom-id
           :initarg :dom-id
           :initform (js:gen-js-name-string :prefix "_ucw_"))
   (client-value :accessor client-value :initarg :client-value
                 :initform ""
                 :documentation "The string the client submitted along with this field.")
   (name :accessor name :initarg :name :initform nil)))

(defmethod print-object ((field generic-html-input) stream)
  (print-unreadable-object (field stream :type t :identity t)
    (format stream "~S" (client-value field))))

(defmethod render :after ((field generic-html-input))
  (when (validators field)
    (<ucw:script
     `(setf (slot-value (document.get-element-by-id ,(dom-id field)) 'ucw-validate)
            (lambda ()
              ,@(loop
                   for validator in (validators field)
                   collect (generate-javascript field validator))))
     `(dojo.event.connect
       (document.get-element-by-id ,(dom-id field))
       "onkeyup"
       (lambda (event)
         ((slot-value (document.get-element-by-id ,(dom-id field)) 'ucw-validate))))
     (when (initially-validate field)
       `((slot-value (document.get-element-by-id ,(dom-id field)) 'ucw-validate))))))

(defgeneric value (form-field))

(defmethod value ((field form-field))
  (client-value field))

(defgeneric (setf value) (new-value form-field))

(defmethod (setf value) ((new-value string) (form-field form-field))
  (setf (client-value form-field) new-value))

(defgeneric validp (form-field))

(defmethod validp ((field form-field))
  (loop
     for validator in (validators field)
     when (null (validate field validator))
       collect validator into failed-validators
     finally (return (values (null failed-validators)
                             failed-validators))))

(defmethod validp ((component standard-component))
  "Loops over COMPONENT's slots, checks whethere all slots which
  contain form-field objects are valid."
  (every #'validp
         (remove-if-not #'form-field-p
                        (mapcar (curry #'slot-value component)
                                (mapcar #'mopp:slot-definition-name
                                        (mopp:class-slots (class-of component)))))))

;;;; ** Form Inputs

;;;; *** Textarea inputs

(defclass textarea-field (generic-html-input)
  ((rows :accessor rows :initarg :rows :initform nil)
   (cols :accessor cols :initarg :cols :initform nil)))

(defmethod render ((field textarea-field))
  (<ucw:textarea :id (dom-id field)
                 :name (name field)
                 :accessor (client-value field)
                 :rows (rows field)
                 :cols (cols field)))

;;;; *** Strings

(defclass string-field (generic-html-input)
  ((input-size :accessor input-size
               :initarg :input-size
               :initform nil)))

(defmethod render ((field string-field))
  (<ucw:text :name (name field)
             :id (dom-id field)
             :accessor (client-value field)
             :size (input-size field)))

;;;; **** Strings with in-field labels

(defclass in-field-string-field (string-field)
  ((in-field-label :accessor in-field-label
                   :initarg :in-field-label
                   :initform nil
                   :documentation "This slot, if non-NIL, will be
                   used as an initial field label. An initial
                   field label is a block of text which is placed
                   inside the input element and removed as soon
                   as the user edits the field. Obviously this
                   field is overidden by an initial :client-value
                   argument."))
  (:default-initargs :client-value nil))

(defmethod render ((field in-field-string-field))
  (<ucw:text :id (dom-id field)
             :name (name field)
             :reader  (or (client-value field)
                          (in-field-label field)
                          "")
             :writer (lambda (v)
                       (setf (client-value field) v))
             :size (input-size field)
             :onfocus (js:js*
                       `(unless this.has-changed
                          ,(in-field-on-focus field))))
  (<ucw:script
   (in-field-setup field)))

(defmethod in-field-on-focus ((field in-field-string-field))
  `(setf this.value ""
         this.has-changed t))

(defmethod in-field-setup ((field in-field-string-field))
  `(setf (slot-value (document.get-element-by-id ,(dom-id field)) 'has-changed) nil))

;;;; **** Hidden Strings (passwords)

(defclass password-field (string-field)
  ())

(defmethod render ((field password-field))
  (<ucw:password :name (name field)
                 :id (dom-id field)
                 :accessor (client-value field)
                 :size (input-size field)))

;;;; *** Integers

(defclass integer-field (string-field)
  ())

(defmethod (setf value) ((new-value integer) (field integer-field))
  (let ((*print-radix* nil)
        (*print-base* 10))
    (setf (client-value field) (princ-to-string new-value))))

(defmethod initialize-instance :after ((field integer-field)
                                       &rest initargs)
  (declare (ignore initargs))
  (push (make-instance 'is-an-integer-validator)
        (validators field)))

(defmethod value ((field integer-field))
  (parse-integer (client-value field) :junk-allowed t))

(defclass is-an-integer-validator (validator)
  ())

(defmethod validate ((field integer-field) (validator is-an-integer-validator))
  (parse-integer (client-value field) :junk-allowed t))

(defmethod javascript-check ((field integer-field) (validator is-an-integer-validator))
  `(or (= "" value)
       (not (is-na-n value))))

(defclass integer-range-validator (validator)
  ((min-value :accessor min-value :initarg :min-value :initform nil)
   (max-value :accessor max-value :initarg :max-value :initform nil)))

(defmethod validate ((field integer-field) (validator integer-range-validator))
  (with-slots (min-value max-value)
      validator
    (and (value field)
         (<= (or min-value (1- (value field)))
             (value field)
             (or max-value (1+ (value field)))))))

(defmethod javascript-check ((field integer-field) (validator integer-range-validator))
  (with-slots (min-value max-value)
      validator
    `(or (= "" value)
         ,(cond
           ((and min-value max-value)
            `(and (<= ,min-value value) (<= value ,max-value)))
           (min-value `(<= ,min-value value))
           (max-value `(<= ,max-value value))
           (t t)))))

;;;; *** Dates

(defclass date-field (form-field)
  ((year :accessor year :initform (make-instance 'integer-field
                                                 :input-size 4
                                                 :validators (list
                                                              (make-instance 'integer-range-validator
                                                                             :min-value 0
                                                                             :max-value nil))))
   (month :accessor month :initform (make-instance 'integer-field
                                                   :input-size 2
                                                   :validators (list (make-instance 'integer-range-validator
                                                                                    :min-value 0
                                                                                    :max-value 12))))
   (day :accessor day :initform (make-instance 'integer-field
                                               :input-size 2
                                               :validators (list (make-instance 'integer-range-validator
                                                                                :min-value 0
                                                                                :max-value 31))))))

(defmethod date-ymd ((field date-field))
  (values (value (year field))
          (value (month field))
          (value (day field))))

(defmethod value ((field date-field))
  (multiple-value-bind (year month day)
      (date-ymd field)
    (if (and year month day)
        (encode-universal-time 0 0 0 day month year)
        nil)))

(defclass dmy-date-field (date-field)
  ()
  (:documentation "Date fields which orders the inputs day/month/year"))

(defmethod render ((field dmy-date-field))
  (render (day field))
  (<:as-html "/")
  (render (month field))
  (<:as-html "/")
  (render (year field)))

(defclass mdy-date-field (date-field)
  ()
  (:documentation "Date field which orders the inputs month/day/year"))

(defmethod render ((field mdy-date-field))
  (render (month field))
  (<:as-html "/")
  (render (day field))
  (<:as-html "/")
  (render (year field)))

;;;; *** Select input fields

;;;; We generate lots of strings which are used as the values in
;;;; option tags, this variable, and the integer-to-string function,
;;;; prevent generating large amounts of throw away strings.

(defvar +string-index-cache+
  (map-into (make-array 50
                        :element-type 'string
                        :adjustable t)
            (let ((i -1))
              (lambda ()
                (princ-to-string (incf i))))))

(defun integer-to-string (i)
  (cond
    ((<= (length +string-index-cache+) i)
     (adjust-array +string-index-cache+ (1+ i))
     (setf (aref +string-index-cache+ i) (princ-to-string i)))
    ((null (aref +string-index-cache+ i))
     (setf (aref +string-index-cache+ i) (princ-to-string i)))
    (t
     (aref +string-index-cache+ i))))

(defclass select-field (generic-html-input)
  ((data-set :accessor data-set :initarg :data-set
             :documentation "The values this select chooses
             from.")
   (data-map :accessor data-map :initform '()
             :documentation "An alist mapping strings, used in
   the select's options, to value in data-set. This is an
   internal field whcih should only be accessed by render and
   value methods."))
  (:documentation "Form field used for selecting one value from a
  list of available options."))

(defmethod value ((field select-field))
  (cdr (assoc (client-value field) (data-map field) :test #'string=)))

(defgeneric build-data-map (field)
  (:documentation "Given FIELD's data-set returns an alist
  mapping strings to the values in FIELD's data-set. Each key in
  the returned alist must be unique."))

(defmethod build-data-map ((field select-field))
  (loop
     for value in (data-set field)
     for index upfrom 0
     collect (cons (integer-to-string index) value)))

(defmethod render ((field select-field))
  (<:select :name (make-new-callback
                   (lambda (value)
                     (setf (client-value field) value)))
    (setf (data-map field) (build-data-map field))
    (render-options field)))

(defgeneric render-options (select-field)
  (:documentation "Function used to render the list of options of
  a select field. Methods are always called AFTER the data-map
  has been setup."))

(defmethod render-options ((field select-field))
  (dolist* ((key-string . value) (data-map field))
    (<:option :value key-string
              :selected  (string= (client-value field) key-string)
              (render-value field value))))

(defgeneric render-value (select-field value)
  (:documentation "This function will be passed each value in the field's
   data-set and must produce the body of the corresponding
   <ucw:option tag."))

(defmethod render-value ((field select-field) (value t))
  "This default method just wraps princ-to-string in a <:as-html."
  (<:as-html value))

;;;; **** selecting from mappings

;;;; It often happens that you want to select a value based on the
;;;; corresponding key.

(defclass mapping-select-field (select-field)
  ()
  (:documentation "Class used when we want to chose the values of
  a certain mapping based on the keys. We render the keys in the
  select and return the corresponding value from the VALUE
  method."))

(defmethod render-options ((field mapping-select-field))
  (dolist* ((key-string . key) (data-map field))
    (<:option :value key-string
              :selected  (string= (client-value field) key-string)
              (render-key field key))))

(defgeneric render-key (field key)
  (:method ((field mapping-select-field) (key t))
    (<:as-html key)))

(defclass hash-table-select-field (mapping-select-field)
  ())

(defmethod build-data-map ((field hash-table-select-field))
  (loop
     for key being the hash-keys of (data-set field)
     for index upfrom 0
     collect (cons (integer-to-string index) key)))

(defmethod value ((field hash-table-select-field))
  (gethash (call-next-method) (data-set field)))

(defclass alist-select-field (mapping-select-field)
  ((test-fn :accessor test-fn :initarg :test :initform #'eql)))

(defmethod build-data-map ((field alist-select-field))
  (loop
     for (key) in (data-set field)
     for index upfrom 0
     collect (cons (integer-to-string index) key)))

(defmethod value ((field alist-select-field))
  (cdr (assoc (call-next-method) (data-set field) :test (test-fn field))))

(defclass plist-select-field (mapping-select-field)
  ())

(defmethod build-data-map ((field plist-select-field))
  (loop
     for (key) on (data-set field) by #'cddr
     for index upfrom 0
     collect (cons (integer-to-string index) key)))

(defmethod value ((field plist-select-field))
  (getf (data-set field) (call-next-method)))

;;;; *** Check box

(defclass checkbox-field (generic-html-input)
  ()
  (:default-initargs :client-value nil))

(defmethod render ((field checkbox-field))
  (let ((value (client-value field)))
    (setf (client-value field) nil)
    (<ucw:input :id (dom-id field)
                :checked (if value t nil)
                :name (name field)
                :reader value
                :writer (lambda (v) (setf (client-value field) v))
                :type "checkbox")))

(defmethod value ((field checkbox-field))
  (if (and (client-value field)
           (not (string= "" (client-value field))))
      t
      nil))

(defmethod (setf value) (new-value (field checkbox-field))
  (setf (client-value field) (if new-value
                                 t
                                 nil)))

;;;; *** Upload

(defclass file-upload-field (generic-html-input)
  ())

(defmethod render ((field file-upload-field))
  (<ucw:input :id (dom-id field)
              :name (name field)
              :type "file"
              :accessor (client-value field)))

;;;; *** Submit Button

(defclass submit-button (generic-html-input)
  ((label :accessor label :initform nil :initarg :label)))

(defmethod render ((field submit-button))
  (<:submit :id (dom-id field)
            :value (label field)))

;;;; ** Validators

(defclass validator ()
  ())

;;;; There are three main parts to the validating api:

;;;; 1 - a javascript function which checks if the current value is valid or not.

;;;; 2 - two javascript functions which handle the valid/invalid cases

;;;; 3 - a lisp function whcih checks if the current value is valid or not.

(defgeneric validate (field validator))

;;;; *** javascript-check

;;;; This pair of generic functions generate the javascript code for
;;;; checking whether the current value of a field is valid or
;;;; not. The top-level function is generate-javascript-check, 

(defgeneric generate-javascript (field validator))

(defmethod generate-javascript ((field generic-html-input) (validator validator))
  `(if (,(generate-javascript-check field validator))
       (,(generate-javascript-valid-handler field validator)
         (document.get-element-by-id ,(dom-id field)))
       (progn
         (,(generate-javascript-invalid-handler field validator)
           (document.get-element-by-id ,(dom-id field)))
         (return nil))))

(defgeneric generate-javascript-check (field validator)
  (:documentation "Generates the javascript code to check the
validaty of FIELD. Methods defined on this generic funcition must
return a one argument javascript lambda. The returned lambda must
return true if the value passed is valid, false otherwise.

This method is built upon JAVASCRIPT-CHECK, which is what you'll
use 90% of the time."))

(defmethod generate-javascript-check ((field t) (validator t))
  `(lambda ()
     (let ((value (slot-value (document.get-element-by-id ,(dom-id field)) 'value)))
       (return ,(javascript-check field validator)))))

(defgeneric javascript-check (field validator)
  (:documentation "Generate javascript code for checknig FIELD against VALIDATOR.

This is the convenience entry point to generate-javascript-check,
methods defined on this generic funcition should return a list of
javascript code (as per parenscript) which tests against the
javascript variable value."))

(defmethod javascript-check ((field form-field) (validator validator))
  t)

;;;; *** javascript-invalid-handler

(defgeneric generate-javascript-invalid-handler (field validator))

(defmethod generate-javascript-invalid-handler ((field t) (validator validator))
  `(lambda (element)
     ,(javascript-invalid-handler field validator)))

(defgeneric javascript-invalid-handler (field validator))

(defmethod javascript-invalid-handler ((field t) (validator validator))
  `(unless (dojo.html.has-class (document.get-element-by-id ,(dom-id field)) "ucw-form-field-invalid")
     (dojo.html.add-class (document.get-element-by-id ,(dom-id field))
                          "ucw-form-field-invalid")))

;;;; *** javascript-valid-handler

(defgeneric generate-javascript-valid-handler (field validator))

(defmethod generate-javascript-valid-handler ((field t) (validator validator))
  `(lambda (element)
     ,(javascript-valid-handler field validator)))

(defgeneric javascript-valid-handler (field validator))

(defmethod javascript-valid-handler ((field t) (validator validator))
  `(progn
     (dojo.html.remove-class (document.get-element-by-id ,(dom-id field))
                             "ucw-form-field-invalid")
     (dojo.html.add-class (document.get-element-by-id ,(dom-id field))
                          "ucw-form-field-valid")))

;;;; ** Standard Form Validators

;;;; *** length

(defclass length-validator (validator)
  ((min-length :accessor min-length :initarg :min-length
               :initform nil)
   (max-length :accessor max-length :initarg :max-length
               :initform nil)))

(defmethod javascript-check ((field form-field) (validator length-validator))
  (with-slots (min-length max-length)
      validator
    (cond
      ((and min-length max-length)
       `(and (<= ,min-length value.length)
             (<= value.length ,max-length)))
      (min-length
       `(<= ,min-length value.length))
      (max-length
       `(<= value.length ,max-length))
      (t t))))

(defmethod validate ((field form-field) (validator length-validator))
  (with-slots (min-length max-length)
      validator
    (let ((length (length (client-value field))))
      (cond
        ((and min-length max-length)
         (<= min-length length max-length))
        (min-length
         (<= min-length length))
        (max-length
         (<= length max-length))
        (t t)))))

;;;; *** not empty

(defclass not-empty-validator (validator)
  ())

(defmethod validate ((field form-field) (validator not-empty-validator))
  (and (client-value field)
       (not (string= "" (client-value field)))))

(defmethod javascript-check ((field form-field) (validator not-empty-validator))
  `(not (= "" value)))

;;;; *** same (string=) value

(defclass string=-validator (validator)
  ((other-field :accessor other-field :initarg :other-field))
  (:documentation "Ensures that a field is string= to another one."))

(defmethod validate ((field form-field) (validator string=-validator))
  (let ((value (value field))
        (other-value (value (other-field validator))))
    (and value other-value
         (string= value other-value))))

(defmethod javascript-check ((field form-field) (validator string=-validator))
  `(= (slot-value (document.get-element-by-id ,(dom-id field)) 'value)
      (slot-value (document.get-element-by-id ,(dom-id (other-field validator))) 'value)))

;;;; *** simple e-mail address check

(defclass e-mail-address-validator (validator)
  ())

(defmethod validate ((field form-field) (validator e-mail-address-validator))
  (cl-ppcre:scan "^([a-zA-Z0-9_\\.\\-])+\\@[a-zA-Z](([a-zA-Z0-9\\-])+\\.)+([a-zA-Z0-9]{2,4})+$"
                 (trim-string (value field))))

(defmethod javascript-check ((field form-field) (validator e-mail-address-validator))
  `(.test (regex "^([a-zA-Z0-9_\\.\\-])+\\@[a-zA-Z](([a-zA-Z0-9\\-])+\\.)+([a-zA-Z0-9]{2,4})+$")
          value))

;;;; ** Simple Form

;;;; This isn't technically part of the form library, but it is
;;;; convenient. Note that a SIMPLE-FORM, unlike the widgets in the
;;;; rest of this file, is a component and can be call'd and answer
;;;; like regular components.

(defcomponent simple-form (widget-component)
  ((submit-method :accessor submit-method
                  :initform "POST"
                  :initarg :sumbit-method)
   (dom-id :accessor dom-id
           :initform (js:gen-js-name-string :prefix "_ucw_simple_form_")
           :initarg :dom-id))
  (:default-initargs :css-id "ucw-simple-form"))

(defmethod render :wrapping ((form simple-form))
  (<ucw:form :id (dom-id form)
	     :method (submit-method form)
             :action (refresh-component form)
    (call-next-method)))


