;; -*- lisp -*-

(in-package :it.bese.ucw)

;;;; ** Container

(defclass container ()
  ((contents :accessor container.contents :initform '()
             :initarg :contents
             :documentation "An alist of (label . component) holding the controlled components.")
   (current-component-name :accessor container.current-component-name
                           :initarg :current-component-name
                           :backtrack t
                           :documentation "The label of the current component."
                           :initform nil)
   (label-test :accessor container.label-test
               :documentation "Function used to compare two labels."
               :initarg :label-test
               :initform #'eql))
  (:metaclass standard-component-class)
  (:documentation "Allow multiple components to share the same place.

The container component serves to manage a set of components
which share the same place in the UI. Of the components under the
containers control only one (the current-component) is generally
rendered.

The container component class is generally used as the super
class for navigatation components and tabbed-pane like
components.

Each contained component has a \"label\" associated with it which
is used to retrieve a particular component and switch
components. Labels are compared with container.label-test.

The :contents inintarg, if provided, must be a list (tag .
component)."))

(defmethod shared-initialize :after ((c container) slot-names
                                     &rest initargs
                                     &key contents)
  "This method sets up any initial contenst for backtacking. If
the contents are created via (setf find-component) then the
backtracking is done there."
  (declare (ignore initargs slot-names))
  (setf (container.contents c) nil)
  (dolist* ((label . comp) contents)
    (setf (find-component c label) comp)))

(defmethod find-component ((c container) label)
  "Returns the component object in C associated with LABEL."
  (if-bind comp (cdr (assoc label (container.contents c)
                            :test (container.label-test c)))
      (values comp t)
      (values nil nil)))

(defmethod (setf find-component) ((component component)
                                  (container container)
                                  label)
  "Associates LABEL with COMPONENT in the container CONTAINER."
  (with-slots (contents label-test) container
    (let ((index (position label contents :test label-test :key #'car)))
      (unless index
        (setf index (list-length contents)))
      (setf (component-at container index label) component)))
  component)

(defmethod add-component ((container container) (component component) &optional label)
  "Add component at the end of the component list."
  (setf (component-at container (list-length (container.contents container)) label)
        component))

(defmethod component-at ((container container) (index integer))
  "Returns the component object in CONTAINER associated at the given INDEX."
  (let ((contents (container.contents container)))
    (if (< index (list-length contents))
        (values (cdr (nth index contents)) t)
        (values nil nil))))

(defmethod (setf component-at) ((component component)
                                (container container)
                                (index integer) &optional label)
  "Associates LABEL with COMPONENT in the container CONTAINER at the given index.
   If label is not provided use the index as label. (setf (c 0 \"optinal-label\") x)"
  (unless label
    (setf label index))
  (let ((contents (container.contents container)))
    (cond
      ((< index (list-length contents))
       (let ((comp-cons (car (nthcdr index contents))))
         (setf (parent (cdr comp-cons)) nil
               (cdr comp-cons) component
               (component.place component) (make-place (cdr comp-cons)))))
      ((= index (list-length contents))
       (let* ((container-cons (cons label component))
              (place (make-place (cdr container-cons))))
         (setf (component.place component) place)
         (if contents
             (nconc contents (list container-cons))
             (setf (container.contents container) (list container-cons)))
         (backtrack (context.current-frame *context*) place)))
      (t (error "Can't set component at index ~A (container size is ~A)"
                index (list-length contents))))
    (setf (parent component) container))
  component)

(defmacro initialize-container ((container &key label-test current-component)
				&body contents)
  (rebinding (container label-test current-component)
    `(setf ,@(iterate
	       (for (label component-type . initargs) in contents)
	       (collect `(find-component ,container ,label))
	       (collect `(make-instance ',component-type ,@initargs)))
	   (container.label-test ,container) (or ,label-test (container.label-test ,container))
	   (container.current-component-name ,container) (or ,current-component
							     ',(first (car (last contents)))))))

(defmethod container.current-component ((container container))
  (find-component container (container.current-component-name container)))

(defmethod ensure-valid-component-label ((container container) label)
  "Returns T is LABEL names one of the component in CONTAINER.

Otherwise a restartable error is signaled."
  (if (find-component container label)
      t
      (restart-case
          (error "No component named ~S in container ~S [~S]."
                 label container (container.contents container))
        (use-first ()
          :report (lambda (stream)
                    (format stream "Use the first component in the container (~S)"
                            (cdr (first (container.contents container)))))
          (cdr (first (container.contents container)))))))

(defaction switch-component ((container container) label)
  (ensure-valid-component-label container label)
  (setf (container.current-component-name container) label))

(defmethod map-contents (func (container container))
  (mapcar (lambda (comp-spec)
            (funcall func (car comp-spec) (cdr comp-spec)))
          (container.contents container)))

(defclass simple-container (container)
  ()
  (:metaclass standard-component-class)
  (:documentation "A simple renderable container component.

This component is exactly like the regular CONTAINER but provides
an implementation of RENDER which simply renders its current
component."))

(defmethod render ((container simple-container))
  (render (container.current-component container)))

(defclass list-container (container widget-component)
  ()
  (:metaclass standard-component-class)
  (:default-initargs :css-class "list-container")
  (:documentation "A simple renderable container component.

This component is exactly like the regular CONTAINER but provides
an implementation of RENDER which renders its contents in <:ol/<:li tags"))

(defmethod initialize-instance ((instance list-container) &rest initargs
                                &key (orientation :horizontal)
                                &allow-other-keys)
  (declare (ignore initargs))
  (unless (or (eql orientation :horizontal)
              (eql orientation :vertical))
    (error "Illegal orientation ~A" orientation))
  (call-next-method)
  (let ((orientation-css-class (if (eql orientation :horizontal)
                                   "horizontal"
                                   "vertical"))
        (css-class (widget-component.css-class instance)))
    (if (listp css-class)
      (setf css-class (append css-class (list orientation-css-class)))
      (setf css-class (list css-class orientation-css-class)))
    (setf (widget-component.css-class instance) css-class)))

(defmethod render ((container list-container))
  (<:ol (iter (for (nil . component) in (container.contents container))
              (<:li (render component)))))

;; Copyright (c) 2003-2005 Edward Marco Baringer
;; All rights reserved. 
;; 
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions are
;; met:
;; 
;;  - Redistributions of source code must retain the above copyright
;;    notice, this list of conditions and the following disclaimer.
;; 
;;  - Redistributions in binary form must reproduce the above copyright
;;    notice, this list of conditions and the following disclaimer in the
;;    documentation and/or other materials provided with the distribution.
;; 
;;  - Neither the name of Edward Marco Baringer, nor BESE, nor the names
;;    of its contributors may be used to endorse or promote products
;;    derived from this software without specific prior written permission.
;; 
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
;; A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT
;; OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;; SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
;; LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
;; THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
;; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
;; OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
