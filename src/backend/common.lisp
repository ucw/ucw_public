;;; -*- lisp -*-

(in-package :it.bese.ucw)

;;;; Library of functions shared by multiple backends.

(defconstant +Space+ #.(char-code #\Space))
(defconstant +Tab+ #.(char-code #\Tab))
(defconstant +Colon+ #.(char-code #\:))
(defconstant +Linefeed+ 10)
(defconstant +Carriage-Return+ 13)

(defun make-displaced-array (array &optional (start 0) (end (length array)))
  (make-array (- end start)
              :element-type (array-element-type array)
              :displaced-to array
              :displaced-index-offset start))

(defun split-on-space (line)
  "Split line on #\Space."
  (let ((parts '()))
    (loop
       with start = 0
       for end upfrom 0
       for char across line
       when (= +Space+ char)
       do (push (make-displaced-array line start end) parts)
       and do (setf start (1+ end))
       finally (push (make-displaced-array line start)
                     parts))
    (nreverse parts)))

(defun split-header-line (line)
  (let* ((colon-position (position +Colon+ line :test #'=))
         (name-length colon-position)
         (value-start (1+ colon-position))
         (value-end (length line)))
    ;; skip any leading space char in the header value
    (iterate
      (for start upfrom value-start)
      (while (< start value-end))
      (while (or (= +Space+ (aref line start))
                 (= +Tab+ (aref line start))))
      (incf start)
      (finally (setf value-start (1- start))))
    (cons (make-displaced-array line 0 name-length)
          (make-displaced-array line value-start value-end))))

;;;; Parsing HTTP request bodies.

;;;; The httpd, mod_lisp and araneida backends use this code.

(defun grab-param (param-string start =-pos end)
  "Returns (KEY . VALUE) of the request param whose first char
  is at START, whose \#= char is at =-POS and whose last char is
  at (1+ END.).

  =-POS may be NIL, END may be equal to =-POS or the last index
  of START."
  (let* ( ;; the index of the first char of the key
         (key-start start)
         ;; the index of the char immediatly after the last char of key
         (key-end (or =-pos end))
         (key (if (position #\% param-string :test #'char= :start key-start :end key-end)
                  (subseq param-string key-start key-end)
                  (make-displaced-array param-string key-start key-end)))
         ;; the index of the first char of the value
         (value-start (if =-pos
                          (1+ =-pos)
                          end))
         ;; the index of the char immediatly after the
         ;; end of the value (may be equal to
         ;; key-start in the case of "" values).
         (value-end end)
         (value (cond
                   ((and value-end
                         (position #\% param-string :test #'char= :start value-start :end value-end))
                    (subseq param-string value-start value-end))
                   ((and value-end
                         (position #\+ param-string :test #'char= :start value-start :end value-end))
                    (loop
                       with value = (make-displaced-array param-string value-start value-end)
                       for char across value
                       for index upfrom 0
                       when (char= #\+ char)
                       do (setf (aref value index) #\Space)
                       finally (return value)))
                   (value-end
                    (make-displaced-array param-string value-start value-end))
                   (t "")))
         (unescaped-key (unescape-as-uri key (external-format-for :url)))
         (unescaped-value (unescape-as-uri value (external-format-for :url))))
    (ucw.backend.dribble "Grabbed parameter ~S with value ~S." unescaped-key unescaped-value)
    (cons unescaped-key unescaped-value)))

(defun parse-query-parameters (param-string)
  (let ((params '()))
    (when (and param-string (< 0 (length param-string)))
      (iterate
        (with start = 0)
        (with =-pos = nil)
        (for char in-vector param-string)
        (for offset upfrom 0)
        (case char
          (#\& ;; end of the current param
           (push (grab-param param-string start =-pos offset) params)
           (setf start (1+ offset)
                 =-pos nil))
          (#\= ;; end of name
           (setf =-pos offset)))
        ;; automatic end of param string
        (finally (push (grab-param param-string start =-pos (1+ offset)) params))))
    (nreverse params)))

(defun make-temp-filename (&optional (prefix "ucw"))
  "Returns a pathname for a temporary file.

This function is used by the rfc2388 parser callback to decide
where to put the mime data."
  (strcat "/tmp/" prefix (princ-to-string (get-universal-time))))

(defun rfc2388-callback (mime-part)
  (ucw.backend.dribble "Processing mime part ~S." mime-part)
  (let* ((header (rfc2388:get-header mime-part "Content-Disposition"))
         (disposition (rfc2388:header-value header))
         (name (rfc2388:get-header-attribute header "name"))
         (filename (rfc2388:get-header-attribute header "filename")))
    (ucw.backend.dribble "Got a mime part. Disposition: ~S; Name: ~S; Filename: ~S" disposition name filename)
    (ucw.backend.dribble "Mime Part: ---~S---~%" (with-output-to-string (dump)
                                                   (rfc2388:print-mime-part mime-part dump)))
    (cond
      ((or (string-equal "file" disposition)
           (not (null filename)))
       (let ((filename (make-temp-filename)))
         (setf (rfc2388:content mime-part) (open filename
                                                 :direction :output
                                                 :element-type '(unsigned-byte 8)))
         (ucw.backend.dribble "Sending mime part data to file ~S (~S)."
			      filename (rfc2388:content mime-part))
         (let ((counter 0))
           (values (lambda (byte)
                     (ucw.backend.dribble "File byte ~4,'0D: ~D~:[~; (~C)~]"
					  counter byte (<= 32 byte 127)
					  (code-char byte))
                     (incf counter)
                     (write-byte byte (rfc2388:content mime-part)))
                   (lambda (mime-part)
                     (ucw.backend.dribble "Done with file ~S."
					  (rfc2388:content mime-part))
                     (close (rfc2388:content mime-part))
                     (setf (rfc2388:content mime-part)
                           (open filename
                                 :direction :input
                                 :element-type '(unsigned-byte 8)))
                     (cons name mime-part))))))
      ((string-equal "form-data" disposition)
       (ucw.backend.dribble "Grabbing mime-part data as string.")
       (setf (rfc2388:content mime-part) (make-array 10
                                                     :element-type 'character
                                                     :adjustable t
                                                     :fill-pointer 0))
       (let ((counter 0))
         (values (lambda (byte)
                   (ucw.backend.dribble "Form-data byte ~4,'0D: ~D~:[~; (~C)~]."
					counter byte (<= 32 byte 127)
					(code-char byte))
                   (incf counter)
                   (vector-push-extend (code-char byte) (rfc2388:content mime-part)))
                 (lambda (mime-part)
                   (ucw.backend.dribble "Done with form-data ~S: ~S"
                                        name (rfc2388:content mime-part))
                   (cons name (rfc2388:content mime-part))))))
      (t
       (error "Don't know how to handle the mime-part ~S (disposition: ~S)"
              mime-part header)))))

(defun parse-request-body (stream content-length content-type attributes)
  (unless (zerop content-length)
    (eswitch (content-type :test #'string=)
      ("application/x-www-form-urlencoded"
       (let ((buffer (make-array content-length :element-type '(unsigned-byte 8))))
         (read-sequence buffer stream)
         (parse-query-parameters
          (switch ((cdr (assoc "charset" attributes :test #'string=))
                   :test #'string=)
            ("ASCII" (octets-to-string buffer :us-ascii))
            ("UTF-8" (octets-to-string buffer :utf-8))
            (t (octets-to-string buffer :iso-8859-1))))))
      ("multipart/form-data"
       (let ((boundary (cdr (assoc "boundary" attributes :test #'string=))))
         (rfc2388:read-mime stream boundary #'rfc2388-callback))))))

(defmethod mime-part-body ((mime-part rfc2388:mime-part))
  (rfc2388:content mime-part))

(defmethod mime-part-headers ((mime-part rfc2388:mime-part))
  (mapcar (lambda (header)
            (cons (rfc2388:header-name header)
                  (rfc2388:header-value header)))
          (rfc2388:headers mime-part)))

;; Copyright (c) 2003-2006 Edward Marco Baringer
;; All rights reserved. 
;; 
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions are
;; met:
;; 
;;  - Redistributions of source code must retain the above copyright
;;    notice, this list of conditions and the following disclaimer.
;; 
;;  - Redistributions in binary form must reproduce the above copyright
;;    notice, this list of conditions and the following disclaimer in the
;;    documentation and/or other materials provided with the distribution.
;; 
;;  - Neither the name of Edward Marco Baringer, nor BESE, nor the names
;;    of its contributors may be used to endorse or promote products
;;    derived from this software without specific prior written permission.
;; 
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
;; A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT
;; OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;; SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
;; LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
;; THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
;; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
;; OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
