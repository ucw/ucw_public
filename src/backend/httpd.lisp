;; -*- lisp -*-

(in-package :it.bese.ucw)

;;;; ** A Trivial HTTP Server

;;;; We don't actually expect anyone to use this backend but 1) it's
;;;; convenient when getting starting and 2) the mod_lisp backend
;;;; reuses most of it.

(eval-when (:compile-toplevel :load-toplevel :execute)
  (defclass httpd-backend (backend)
    ((port :accessor port :initarg :port :initform 8080)
     (host :accessor host :initarg :host :initform "127.0.0.1")
     (socket :accessor socket)
     (server :accessor server :initarg :server)
     (handlers :accessor handlers :initform '())))
  
  (defclass httpd-message (message)
    ((headers :accessor headers :initform '())
     (network-stream :accessor network-stream :initarg :network-stream)))

  (defclass httpd-request (httpd-message request)
    ((parameters :accessor parameters :initform '())
     (raw-uri :accessor raw-uri :initform nil)
     (query-path :accessor query-path :initform nil)
     (raw-body :accessor raw-body :initform nil)
     (http-method :accessor http-method :initform nil)))

  (defclass httpd-response (httpd-message response)
    ((html-stream :accessor html-stream :initform (make-string-output-stream))
     (status :accessor status :initform "200 OK")
     (external-format :accessor external-format :initform nil))))

;;;; mapping MIME types to extensions
(defvar *mime-types* nil)
(defvar *default-mime-types-file* #P"/etc/mime.types")

(defun parse-mime-types (mime-types-file)
  "Parse mime.types file"
  (iterate
    (for line in-file mime-types-file using #'read-line)
    (when (or (string= "" line) (eq #\# (aref line 0)))
      (next-iteration))
    (for splut = (split-sequence:split-sequence-if
		  #'(lambda (char)
		      (member char '(#\Space #\Tab)))
		  line :remove-empty-subseqs t))
    (unless (null splut)
      (collect splut))))

(defun read-mime-types (mime-types-file)
  "Read in mime.types file."
  (iterate
    (for (type . extensions)
	  in (parse-mime-types mime-types-file))
    (aif (assoc type *mime-types* :test #'string-equal)
	 (setf (cdr it) (nconc (cdr it) extensions))
	 (push (cons type extensions) *mime-types*))))

(defun mime-type-extensions (type)
  "Extensions that can be given to file of given MIME type."
  (cdr (assoc type *mime-types* :test #'string-equal)))

(defun extension-mime-type (extension)
  "MIME type associated with file extension."
  (first
   (find-if #'(lambda (typespec)
		(find extension (rest typespec)
		      :test #'string-equal))
	    *mime-types*)))

;;;; Backend methods
(defmethod initialize-backend ((backend httpd-backend) &key server &allow-other-keys)
  (when (and (null *mime-types*) (probe-file *default-mime-types-file*))
    (read-mime-types *default-mime-types-file*))
  (setf (server backend) server)
  backend)

(defmethod startup-backend :before ((backend httpd-backend) &rest initargs)
  (declare (ignore initargs))
  (setf (socket backend) (open-server :host (host backend) :port (port backend))))

(defmethod register-url-handler ((backend httpd-backend) (entry-point standard-entry-point)
                                 handler-lambda)
  (let ((complete-url (strcat (application.url-prefix
                               (entry-point.application entry-point))
                              (entry-point.url entry-point))))
    (push (list (lambda (request-url)
                  (string= request-url complete-url))
                handler-lambda
                entry-point)
          (handlers backend))))

(defmethod register-url-handler ((backend httpd-backend) (entry-point regexp-entry-point)
                                 handler-lambda)
  (push (list (lambda (request-url)
                (let ((url-prefix (application.url-prefix (entry-point.application entry-point))))
                  (and (starts-with request-url url-prefix)
                       (cl-ppcre:scan (entry-point.url entry-point)
                                      (subseq request-url (length url-prefix))))))
              handler-lambda
              entry-point)
        (handlers backend)))

(defmethod unregister-url-handler ((backend httpd-backend) (entry-point standard-entry-point))
  (setf (handlers backend) (delete entry-point (handlers backend) :key #'third)))

(defmethod handle-request ((backend httpd-backend) (request httpd-request) (response httpd-response))
  (ucw.backend.info "Handling request for ~S" (query-path request))
  (dolist* ((can-handle-check handler ep) (handlers backend))
    (declare (ignore ep))
    (when (funcall can-handle-check (query-path request))
      (return-from handle-request (funcall handler request response))))
  ;; if we get here there's no handler defined for the request
  (handle-404 backend request response))

(defmethod handle-404 ((backend httpd-backend) (request httpd-request) (response httpd-response))
  (setf (get-header response "Status") "404 Not Found"
        (get-header response "Content-Type") "text/html")
  (with-yaclml-stream (html-stream response)
    (<:html
     (<:head (<:title "404 Not Found"))
     (<:body
      (<:p (<:as-html (raw-uri request)) " not found."))))
  (shutdown request)
  (shutdown response))

(defmethod send-error ((stream stream) message)
  "Ignore trying to read the request or anything. Just send an
error message. This is a very low level bailout function."
  (let ((response (make-response stream)))
    (setf (get-header response "Status") "500 Internal Server Error"
          (get-header response "Content-Type") "text/html")
    (with-yaclml-stream (html-stream response)
      (<:html
       (<:head (<:title "500 Internal Server Error"))
       (<:body
        (<:p "Server Error.")
        (<:p (<:as-html message)))))
    (shutdown response)))

;;;; The single threaded server

(defmethod startup-backend ((backend httpd-backend) &rest init-args)
  "Start the RERL."
  (declare (ignore init-args))
  (let (stream request response)
    (labels ((serve-one-request ()
               (setf stream (accept-connection (socket backend) :element-type '(unsigned-byte 8))
                     request (read-request backend stream)
                     response (make-response request))
               (handle-request backend request response))
             (handle-request-error (condition)
               (ucw.backend.error "While handling a request on ~S: ~S" stream condition)
               (when *debug-on-error*
                 (restart-case
                     (swank:swank-debugger-hook condition nil)
                   (kill-worker ()
                     :report "Kill this worker."
                     (values))))
               (throw 'abort-request nil))
             (handle-request/internal ()
               (catch 'abort-request
                 (handler-bind ((error #'handle-request-error))
                   (unwind-protect
                        (serve-one-request)
                     (ignore-errors (close stream)))))))
      (unwind-protect
           (loop (handle-request/internal))
        (ignore-errors
          (swank-backend:close-socket (socket backend))))))
  backend)

(defmethod shutdown-backend ((backend httpd-backend) &rest init-args)
  "This would stop the single therad httpd backend if that made any sense.

Stopping the single therad backend requires nothing more than
getting STARTUP-BACKEND to return (either normally or by chosing
you implementation's abort restart after a break)."
  (declare (ignore init-args))
  backend)

(defun serve-file (filename request response)
  (with-input-from-file (file filename :element-type 'character :external-format :iso-8859-1)
    (setf (get-header response "Status") "200 OK"
          (get-header response "Content-Type") (or (extension-mime-type (pathname-type filename))
						   (switch ((pathname-type filename) :test #'string=)
						     ("html" "text/html")
						     ("css"  "text/css")
						     (t "text/plain"))))
    (loop
       with buffer = (make-array 8192 :element-type 'character)
       for end-pos = (read-sequence buffer file)
       until (zerop end-pos) do
       (write-sequence buffer (html-stream response) :end end-pos)))
  (setf (external-format response) :iso-8859-1)
  (shutdown request)
  (shutdown response))

(defmethod publish-directory ((backend httpd-backend)
                              directory-pathname url-base)
  (push (list (lambda (request-url)
                (map-query-path-to-file request-url url-base directory-pathname))
              (lambda (request response)
                (serve-file (map-query-path-to-file (query-path request)
                                                    url-base
                                                    directory-pathname)
                            request response))
              url-base)
        (handlers backend)))

(defun map-query-path-to-file (query-path url-base directory)
  "Converts QUERY-PATH to a file on the local filesystem assuming
  the application is mapped to URL-BASE and lives in DIRECTORY.

In other words: if query-path is \"<URL-BASE><SOMETHING>\"
returns: \"<DIRECTORY><SOMETHING>\" (assuming said file
exists), otherwise returns NIL."
  ;; NB: We really could (and should) cache this function on
  ;; QUERY-PATH, but we need to figure out how to wipe the cache when
  ;; an application's url-prefix changes. considering how rarely that
  ;; happens it's sucks that we lose the spped increase this could
  ;; bring...
  (multiple-value-bind (starts-with <something>)
      (starts-with query-path url-base :return-suffix t)
    (if starts-with
        (let ((pathname (merge-pathnames <something> directory)))
          (if (probe-file pathname)
              pathname
              nil))
        nil)))

;;;; Message headers methods

(defmethod get-header ((message httpd-message) header-name)
  (cdr (assoc header-name (headers message) :test #'string-equal)))

(defmethod (setf get-header) (value (message httpd-message) header-name)
  (aif (assoc header-name (headers message) :test #'string-equal)
       (setf (cdr it) value)
       (push (cons header-name value) (headers message)))
  value)

(defmethod add-header ((message httpd-message) header-name value)
  (push (cons header-name value) (headers message))
  value)

;;;; Request handling

(defmethod read-line-from-network ((backend httpd-backend) stream)
  "A simple state machine which reads chars from STREAM until it
  gets a CR-LF sequence or the end of the stream."
  (let ((buffer (make-array 50
                            :element-type '(unsigned-byte 8)
                            :adjustable t
                            :fill-pointer 0)))
    (labels ((read-next-char ()
               (let ((byte (read-byte stream nil stream)))
                 (if (eq stream byte)
                     (return-from read-line-from-network buffer)
                     (return-from read-next-char byte))))
             (cr ()
               (let ((next-byte (read-byte stream)))
                 (case next-byte
                   (#.+linefeed+ ;; LF
                      (return-from read-line-from-network buffer))
                   (t ;; add both the cr and this char to the buffer
                    (vector-push-extend +carriage-return+ buffer)
                    (vector-push-extend next-byte buffer)
                    (next)))))
             (next ()
               (let ((next-byte (read-byte stream)))
                 (case next-byte
                   (#.+carriage-return+ ;; CR
                      (cr))
                   (t
                    (vector-push-extend next-byte buffer)
                    (next))))))
      (next)
      buffer)))

(defmethod read-request ((backend httpd-backend) stream)
  "Reads an HTTP request message from STREAM. Returns a httpd-request object."
  (let ((request (make-instance 'httpd-request :network-stream stream)))
    (destructuring-bind (http-method uri &optional protocol)
        (split-on-space (read-line-from-network backend stream))
      (declare (ignore protocol))
      (setf (raw-uri request) (coerce (octets-to-string uri :iso-8859-1) 'simple-string)
            (http-method request) http-method
            (headers request) (read-request-headers backend stream))
      (aif (position #\? (raw-uri request))
           (setf (query-path request) (make-displaced-array (raw-uri request) 0 it)
                 (parameters request) (parse-query-parameters
                                       (make-displaced-array (raw-uri request)
                                                             (1+ it))))
           (setf (query-path request) (raw-uri request)
                 (parameters request) '()))
      (when-bind content-length
          (awhen (get-header request "Content-Length")
            (parse-integer it :junk-allowed t))
        (setf (parameters request) (append (parameters request)
                                           (multiple-value-bind (content-type attributes)
                                               (rfc2388:parse-header-value (get-header request "Content-Type"))
                                             (parse-request-body stream
                                                                 content-length
                                                                 content-type
                                                                 attributes))))))
    request))

(defmethod get-parameter ((request httpd-request) name)
  (when-bind value
      (cdr (assoc name (parameters request) :test #'string=))
    (if (stringp value)
        (copy-seq value)
        value)))

(defmethod map-parameters ((request httpd-request) lambda)
  (dolist* ((name . value) (parameters request))
    (funcall lambda name (if (stringp value)
                             (copy-seq value)
                             value))))

(defun read-request-headers (backend stream)
  (iterate
    (for header-line = (read-line-from-network backend stream))
    (until (= 0 (length header-line)))
    (for (name . value) = (split-header-line header-line))
    (collect (cons (octets-to-string name :us-ascii)
                   (octets-to-string value :us-ascii)))))

(defmethod shutdown ((request httpd-request))
  request)

;;;; Response objects

(defmethod make-response ((request httpd-request))
  (make-response (network-stream request)))

(defmethod make-response ((stream stream))
  (make-instance 'httpd-response :network-stream stream))

(defmethod clear-response ((response httpd-response))
  (setf (html-stream response) (make-string-output-stream)
        (headers response) '()))

;;;; httpd-response objects special case the "Status" header.

(defmethod get-header ((response httpd-response) header-name)
  (if (string= "Status" header-name)
      (status response)
      (call-next-method)))

(defmethod (setf get-header) (value (response httpd-response) header-name)
  (if (string= "Status" header-name)
      (setf (status response) value)
      (call-next-method)))

(defun write-crlf (stream)
  (write-byte 13 stream)
  (write-byte 10 stream))

(defun write-header-line (name value stream)
  (write-sequence (string-to-octets name :us-ascii) stream)
  ;; ": "
  (write-byte 58 stream)
  (write-byte 32 stream)
  (write-sequence (string-to-octets value :us-ascii) stream)
  (write-crlf stream))

(defmethod shutdown ((response httpd-response))
  (ucw.backend.info "Shutdown down ~S (Status: ~S)." response (status response))
  (let ((stream (network-stream response)))
    (write-sequence #.(string-to-octets "HTTP/1.1 " :us-ascii) stream)
    (write-sequence (string-to-octets (status response) :us-ascii) stream)
    (write-byte 32 stream)
    (write-crlf stream)
    (dolist* ((name . value) (headers response))
      (ucw.backend.dribble "Sending header ~S: ~S" name value) 
      (write-header-line name value stream))
    (let ((content (string-to-octets (get-output-stream-string (html-stream response))
                                     (or (external-format response)
                                         (awhen (and (not (eq *context* :unbound))
                                                     (context.application *context*))
                                                (application.charset it))
                                         (external-format-for :http)
                                         :iso-8859-1))))
      (ucw.backend.dribble "HTTPD: Sending ~S (~D bytes) as body" content (length content))
      (write-header-line "Content-Length" (princ-to-string (length content)) stream)
      (write-crlf stream)
      (write-sequence content stream))
    response))

;;;; Debugging the backend

(defparameter *httpd-trace-functions*
  '(swank-backend:send
    swank-backend:spawn
    swank-backend:receive
    initialize-backend
    startup-backend
    shutdown-backend
    shutdown
    httpd-controller-loop
    httpd-worker-loop/handle
    httpd-worker-loop
    httpd-accept-loop
    get-header
    (setf get-header)
    add-header
    read-request
    parse-query-parameters
    parse-request-body
    rfc2388-callback
    grab-param
    read-line-from-network
    split-on-space
    split-header-line
    read-request-headers
    make-response
    get-parameter
    map-parameters
    rfc2388:parse-header-value
    rfc2388:get-header))

(defun trace-httpd-backend ()
  (eval `(trace ,@*httpd-trace-functions*)))

(defun untrace-httpd-backend ()
  (eval `(untrace ,@*httpd-trace-functions*)))

;; Copyright (c) 2005-2006 Edward Marco Baringer
;; All rights reserved. 
;; 
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions are
;; met:
;; 
;;  - Redistributions of source code must retain the above copyright
;;    notice, this list of conditions and the following disclaimer.
;; 
;;  - Redistributions in binary form must reproduce the above copyright
;;    notice, this list of conditions and the following disclaimer in the
;;    documentation and/or other materials provided with the distribution.
;; 
;;  - Neither the name of Edward Marco Baringer, nor BESE, nor the names
;;    of its contributors may be used to endorse or promote products
;;    derived from this software without specific prior written permission.
;; 
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
;; A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT
;; OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;; SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
;; LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
;; THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
;; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
;; OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
