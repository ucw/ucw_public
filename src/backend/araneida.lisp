;; -*- lisp -*-

(in-package :it.bese.ucw) 

;;;; ** The araneida backend

(deflogger araneida ()
  :level +debug+
  :appender (make-instance 'brief-stream-log-appender :stream t))

(defclass araneida-backend (backend)
  ((listener :accessor listener :initarg :listener :initform nil)
   (default-url :accessor default-url :initarg :default-url)
   (listener-class :accessor listener-class :initarg :listener-class
		   :initform
		   ;; we prefer araneida-serve-event if that's
		   ;; available, otherwise we fall back to araneida
		   ;; threads
		   #+araneida-serve-event
		   'araneida:serve-event-http-listener
		   #+(and (not araneida-serve-event) araneida-threads)
		   'araneida:threaded-http-listener
		   #-(or araneida-threads araneida-serve-event)
		   (error "Can't start araneida in threaded mode nor in event loop.")))
  (:documentation "A UCW backend attached to an araneida server."))

(defclass ucw-handler (araneida:handler)
  ((handler :accessor handler :initarg :handler))) 

(defmethod araneida:handle-request-response ((handler ucw-handler) method request)
  (declare (ignore method))
  (funcall (handler handler)
	   (make-instance 'araneida-request :request request)
	   (make-instance 'araneida-response :request request))
  t)

(defclass ucw-regexp-dispatching-handler (araneida:dispatching-handler)
 ()
 (:documentation "Dispatching handler with multiple match
 capability (exact-p can be a regexp)"))

(defun handler-dispatch-list (handler discriminator)
  "List of ((predicate . handler) (predicate . handler) ...) for ucw-regexp-dispatching-handler"
  (let ((child (assoc discriminator (araneida:child-handlers handler) :test #'string=)))
    (if child
        (values (cdddr child) t)
        (values nil nil))))

(defmethod araneida:install-handler :around
    ((parent ucw-regexp-dispatching-handler) child discriminator exact-p)
  (if (or (null exact-p) (eq t exact-p))
      (call-next-method parent child discriminator exact-p)
      (let*
          ((discriminator (if (typep discriminator 'araneida:url)
                              (araneida:urlstring discriminator)
                              discriminator))
           (existing (assoc discriminator (araneida:child-handlers parent)
                            :test #'string=))
           (existing-sub (assoc exact-p (cdddr existing) :test #'string=)))
        (if existing
            (if existing-sub
                (setf (cdr existing-sub) child)
                (setf (cdr (last existing)) (list (cons exact-p child))))
            (setf (araneida:child-handlers parent)
                  (merge 'list
                         (araneida:child-handlers parent)
                         (list
                          (list discriminator nil nil (cons exact-p child)))
                         #'string> :key #'car)))
        (araneida:child-handlers parent))))

(defmethod araneida:uninstall-handler
    ((parent ucw-regexp-dispatching-handler) discriminator exact-p)
  (let ((existing (assoc discriminator (araneida:child-handlers parent)
                         :test #'string=)))
    (when existing
      (cond
        ((null exact-p) (setf (third existing) nil))
        ((eq t exact-p) (setf (second existing) nil))
        (t (setf (cdddr existing)
                 (remove-if #'(lambda (a) (string= (car a) exact-p))
                            (handler-dispatch-list parent discriminator)))))
      (when (and (null (second existing))
                 (null (third existing))
                 (null (cdddr existing)))
        (setf (araneida:child-handlers parent)
              (remove existing (araneida:child-handlers parent))))))
  (araneida:child-handlers parent))

(defmethod araneida:find-handler :around
    ((parent ucw-regexp-dispatching-handler) discriminator exact-p)
  (if (or (null exact-p) (eq t exact-p))
      (call-next-method parent discriminator exact-p)
      (cdr (assoc exact-p (handler-dispatch-list parent discriminator)
                  :test #'string=))))

(defmethod araneida:handle-request-response
    ((handler ucw-regexp-dispatching-handler) method request)
  (declare (ignore method))
  (let* ((offset (or (second (first (araneida:request-handled-by request))) 0))
         (urlstring (araneida::request-urlstring request))
         (rest-of-url (araneida::request-unhandled-part request))
         (handlers (assoc rest-of-url (araneida:child-handlers handler)
                          :test #'starts-with))
         (offset-delta (length (first handlers)))
         (handler
          (if (and (third handlers)
                   (eql (length rest-of-url) (length (car handlers))))
              (third handlers)
              (or
               (let* ((very-rest-of-url
                       (first (split-sequence:split-sequence
                               #\? (subseq rest-of-url (length (first handlers)))
                               :count 1)))
                      (teh-handler
                       (find-if #'(lambda (handler-spec)
                                    (when (cl-ppcre:scan (car handler-spec) very-rest-of-url)
                                      (cdr handler-spec)))
                                (cdddr handlers))))
                 (when teh-handler
                   (setf offset-delta (length rest-of-url))) ; Should be regexp match length
                                        ; but we won't nest regexp handlers, so it doesn't matter.
                 (cdr teh-handler))
              (second handlers)))))

    (when handler
      (let ((new-offset (+ offset offset-delta)))
        (push (list handler new-offset) (araneida:request-handled-by request))
        (setf (araneida:request-base-url request)
              (araneida:parse-urlstring (subseq urlstring 0 new-offset))))
      (araneida:handle-request handler request))))

(defmethod initialize-instance :after ((backend araneida-backend)
				       &key (port 8080) (host "127.0.0.1"))
  (unless (listener backend)
    (setf (listener backend) (make-instance (listener-class backend)
                                            :handler (make-instance 'ucw-regexp-dispatching-handler)
                                            :port port)
	  (default-url backend) (araneida:make-url :scheme "http" :host host :port port)
	  (araneida::http-listener-default-hostname (listener backend)) host
	  araneida::*default-url-defaults* (default-url backend))))

(defmethod initialize-backend ((backend araneida-backend) &rest init-args)
  (declare (ignore init-args))
  backend)

(defmethod startup-backend ((backend araneida-backend) &rest init-args)
  "Start the RERL."
  (declare (ignore init-args))
  (ucw.backend.info "Starting up ARANEIDA backend ~S on ~A"
		    backend
		    (araneida:urlstring (default-url backend)))
  (araneida:start-listening (listener backend))
  #+clisp (araneida:host-serve-events))

(defmethod shutdown-backend ((backend araneida-backend) &rest init-args)
  (declare (ignore init-args))
  (ucw.backend.info "Stopping ARANEIDA backend ~S on ~A."
		    backend
		    (default-url backend))
  (araneida:stop-listening (listener backend))
  backend) 

(defmethod register-url-handler ((backend araneida-backend) (entry-point standard-entry-point)
                                 handler)
  (ucw.backend.dribble "Registering handler ~S at url ~S." handler (entry-point.url entry-point))
  (araneida:install-handler
   (araneida:http-listener-handler (listener backend))
   (make-instance 'ucw-handler :handler handler)
   (araneida:urlstring
    (araneida:merge-url (default-url backend)
                        (strcat (application.url-prefix
                                 (entry-point.application entry-point))
                                (entry-point.url entry-point))))
   nil))

(defmethod register-url-handler ((backend araneida-backend) (entry-point regexp-entry-point)
                                 handler)
  (ucw.backend.dribble "Registering regexp handler ~S at url ~S." handler (entry-point.url entry-point))
  (araneida:install-handler
   (araneida:http-listener-handler (listener backend))
   (make-instance 'ucw-handler :handler handler)
   (araneida:urlstring
    (araneida:merge-url (default-url backend)
                        (application.url-prefix
                                 (entry-point.application entry-point))))
   (entry-point.url entry-point)))

(defmethod unregister-url-handler ((backend araneida-backend) (entry-point standard-entry-point))
  (ucw.backend.dribble "Unregistering handler for url ~S." (entry-point.url entry-point))
  (araneida:uninstall-handler
   (listener backend)
   (araneida:merge-url (default-url backend)
                       (strcat (application.url-prefix
                                (entry-point.application entry-point))
                               (entry-point.url entry-point)))
   nil))

(defmethod publish-directory ((backend araneida-backend) directory-pathname url-base)
  (ucw.backend.dribble "Publishing local directory ~S at ~S." directory-pathname url-base)
  (araneida:install-handler (araneida:http-listener-handler (listener backend))
			    (make-instance 'araneida:static-file-handler
					   :pathname directory-pathname)
			    (araneida:merge-url (default-url backend) url-base)
			    nil))

;;;; request/response

(defclass araneida-request (request)
  ((request :accessor request :initarg :request)))

(defmethod query-path ((request araneida-request))
  (araneida:url-path (araneida:request-url (request request))))

(defun all-params (request)
  (iterate
    (with request = (request request))
    (with params = (list))
    (initially
     (ucw.backend.dribble "Request header: ~S."
                          (araneida:request-header request :content-type)))
    (for (name . value) in (append
                            (parse-query-parameters (araneida:url-query (araneida:request-url request)))
                            (when (araneida:request-unparsed-body request)
                              (parse-query-parameters (araneida:request-unparsed-body request)))))
    (let ((param (assoc name params :test #'string-equal)))
      (if param
	  (setf (cdr param) (cons value (cdr param)))
	  (push (list name value) params)))
    (finally (return params))))

(defun param-value-or-list (values)
  (if (consp values)
      (if (second values)
	  ;; a list with more than 1 element, pas the whole thing
	  values
	  ;; a list with multiple values
	  (car values))
      ;; a single value, just pass it
      values))

(defmethod get-parameter ((request araneida-request) parameter-name)
  (param-value-or-list
   (cdr (assoc parameter-name (all-params request) :test #'string-equal))))

(defmethod map-parameters ((request araneida-request) lambda)
  (dolist* ((name . values) (all-params request))
    (funcall lambda name (param-value-or-list values))))

(defmethod get-header ((request araneida-request) header-name)
  (flet ((grab-header (header-keyword)
           (first (araneida:request-header (request request) header-keyword))))
    (switch (header-name :test #'string=)
      ("Cookie" (grab-header :cookie))
      ("Referer" (grab-header :referer))
      ("Connection" (grab-header :connection))
      ("Keep-Alive" (grab-header :keep-alive))
      ("Accept-Charset" (grab-header :accept-charset))
      ("Accept-Encoding" (grab-header :accept-encoding))
      ("Accept-Language" (grab-header :accept-language))
      ("Accept" (grab-header :accept))
      ("User-Agent" (grab-header :user-agent))
      ("Host" (grab-header :host))
      (t (grab-header (intern (string-upcase header-name) :keyword))))))

(defmethod shutdown ((r araneida-request))
  nil)

(defclass araneida-response (response)
  ((request :accessor request :initarg :request)
   (headers :accessor headers :initform '())
   (html-stream :accessor html-stream :initform (make-string-output-stream))))

(defmethod clear-response ((response araneida-response))
  (setf (headers response) '()
	(html-stream response) (make-string-output-stream))
  response)

(defmethod (setf get-header) (value (r araneida-response) name)
  (if (assoc name (headers r) :test #'string-equal)
      (setf (cdr (assoc name (headers r) :test #'string-equal)) value)
      (add-header r name value)))

(defmethod add-header ((response araneida-response) header-name value)
  (push (cons header-name value) (headers response)))

(defmethod shutdown ((r araneida-response))
  (let (content-type content-type/charset content-length expires cache-control location refresh
	pragma set-cookie conditional www-authenticate last-modified extra-headers)
  (dolist* ((&whole header-cons name . value) (headers r))
    (switch (name :test #'string-equal)
      ("Content-Type"
       (multiple-value-bind (type attributes)
            (rfc2388:parse-header-value value)
         (setf content-type type)
         (when-bind charset/encoding (assoc "charset" attributes :test #'string=)
           (setf content-type/charset (cdr content-type/charset)))))
      ("Content-Length"   (setf content-length   value))
      ("Expires"          (setf expires          value))
      ("Cache-Control"    (setf cache-control    value))
      ("Location"         (setf location         value))
      ("Refresh"          (setf refresh          value))
      ("Pragma"           (setf pragma           value))
      ("Set-Cookie"       (setf set-cookie       value))
      ("Conditional"      (setf conditional      value))
      ("WWW-Authenticate" (setf www-authenticate value))
      ("Last-Modified"    (setf last-modified    value))
      (t (push header-cons extra-headers))))
  (let ((content (if (starts-with content-type "text")
                     (string-to-octets (get-output-stream-string (html-stream r))
                                       (switch (content-type/charset :test #'string=)
                                         ("UTF-8" :utf-8)
                                         (("latin-1" "iso-8859-1") :iso-8859-1)
                                         (t :us-ascii)))
                     ;; um, it's not text. this is really wrong
                     (string-to-octets (get-output-stream-string (html-stream r))
                                       :iso-8859-1))))
    (araneida:request-send-headers (request r)
				   :response-code (cdr (assoc "Status" (headers r) :test #'string-equal))
				   :content-type (or content-type "text/html")
				   :content-length (or content-length (length content))
				   :expires expires
				   :cache-control cache-control
				   :location location
				   :refresh refresh
				   :pragma pragma
				   :set-cookie set-cookie
				   :conditional conditional
				   :www-authenticate www-authenticate
				   :last-modified last-modified
				   :extra-http-headers extra-headers)
    (write-sequence content (araneida:request-stream (request r))))))

(defmethod make-backend ((backend araneida:http-listener) &key host port)
  (make-instance 'ucw:araneida-backend
		 :listener backend
		 :default-url (araneida:make-url :scheme "http" :host host :port port)))

;; Copyright (c) 2003-2006 Edward Marco Baringer
;; All rights reserved. 
;; 
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions are
;; met:
;; 
;;  - Redistributions of source code must retain the above copyright
;;    notice, this list of conditions and the following disclaimer.
;; 
;;  - Redistributions in binary form must reproduce the above copyright
;;    notice, this list of conditions and the following disclaimer in the
;;    documentation and/or other materials provided with the distribution.
;; 
;;  - Neither the name of Edward Marco Baringer, nor BESE, nor the names
;;    of its contributors may be used to endorse or promote products
;;    derived from this software without specific prior written permission.
;; 
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
;; A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT
;; OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;; SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
;; LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
;; THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
;; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
;; OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
