;;;; -*- lisp -*-

(in-package :it.bese.ucw)

(defpackage :it.bese.ucw.i18n
  (:use)
  (:export))

(defclass i18n-application (standard-application)
  ((default-locale-id :initarg :default-locale-id :accessor application.default-locale-id)
   (accepted-locales :initarg :accepted-locales :accessor application.accepted-locales
                     :initform '()))
  (:documentation "Application class which can handle i18n requests."))

(defclass i18n-request-context (standard-request-context)
  ((locale :accessor context.locale :initarg :locale :initform nil))
  (:documentation "Request context for i18n apps. Contains one
  extra slot: the name (a string) of the locale associated with
  this request."))

(defmethod make-request-context ((app i18n-application) (request request) (response response))
  "Based on the languages the user prefers (as per the
  Accept-Languages header) and the locales APP is able to
  provide, creates a new request context with the effective
  locale."
  (make-instance 'i18n-request-context
                 :request request :response response
                 :application app :locale (or (process-accept-language app request)
                                              (application.default-locale-id app))))

(defmethod process-accept-language ((app i18n-application) (request request))
  (awhen (get-header request :accept-language)
    (let ((langs (parse-accept-language-header it)))
      (dolist (l langs)
        ;; first we check for an exact match
        (when (member (first l) (application.accepted-locales app) :test #'string=)
          (return-from process-accept-language (first l)))
        (when (= 2 (length (first l)))
          ;; they asked for a language without suppyling a script, see
          ;; if we provied a language of that script.
          (dolist (a (application.accepted-locales app))
            (when (string= (first l) (subseq a 2 4))
              (return-from process-accept-language a))))))))

(defclass i18n-tal-generator (file-system-generator)
  ()
  (:documentation "TAL File system generator which maps template
  names to files while considering the locale of the request
  context."))

(defmethod yaclml:template-truename ((generator i18n-tal-generator) (name pathname))
  ;; first see if there's a locale specific version
  (aif (call-next-method 
        generator
        (make-pathname :defaults name
                       :directory (nconc (list :relative (context.locale *context*))
                                         (case (car (pathname-directory name))
                                           (:relative (cdr (pathname-directory name)))
                                           (:absolute (error "Can't pass an absolute pathname to template-truename."))
                                           ((nil) nil)
                                           (t (error "Don't know how to deal with the pathname-directory ~S." 
                                                     (pathname-directory name)))))))
       (return-from yaclml:template-truename it)
       ;; nope, use the default
       (call-next-method)))

(defmethod render-template ((context i18n-request-context) template-name environment)
  (declare (ignore template-name environment))
  (let ((yaclml:*uri-to-package* (cons (cons "http://common-lisp.net/project/ucw/i18n" (find-package :it.bese.ucw.i18n))
                                       yaclml:*uri-to-package*)))
    (call-next-method)))

(defun html-escape-ustring (ustring)
  "Given an ICU ustring return corresponding html string."
  (with-output-to-string (html)
    (loop
       for uchar across ustring
       if (< uchar #xff)
         do (write-char (code-char uchar) html)
       else
         do (format html "&#~4,'0X" uchar))))

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Copyright (c) 2003-2005 Edward Marco Baringer
;;; All rights reserved. 
;;; 
;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions are
;;; met:
;;; 
;;;  - Redistributions of source code must retain the above copyright
;;;    notice, this list of conditions and the following disclaimer.
;;; 
;;;  - Redistributions in binary form must reproduce the above copyright
;;;    notice, this list of conditions and the following disclaimer in the
;;;    documentation and/or other materials provided with the distribution.
;;; 
;;;  - Neither the name of Edward Marco Baringer, nor BESE, nor the names
;;;    of its contributors may be used to endorse or promote products
;;;    derived from this software without specific prior written permission.
;;; 
;;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
;;; A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT
;;; OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;;; SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
;;; LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
;;; THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
;;; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
;;; OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
