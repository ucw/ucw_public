;;;; -*- lisp -*-

(in-package :it.bese.ucw)

;;;; Extra UCW tags and attributes integrated into cl-icu.

(yaclml::def-tag-handler it.bese.ucw.i18n::format (tag)
  (let ((date (getf (cdar tag) 'it.bese.ucw.i18n::date))
        (number (getf (cdar tag) 'it.bese.ucw.i18n::number))
        (style (or (getf (cdar tag) 'it.bese.ucw.i18n::style)
                   ":default")))
    (remf (cdar tag) 'it.bese.ucw.i18n::date)
    (remf (cdar tag) 'it.bese.ucw.i18n::number)
    (remf (cdar tag) 'it.bese.ucw.i18n::style)
    `(it.bese.ucw.i18n::<format ,@(when date (list :date (yaclml:read-tal-expression-from-string date)))
                                ,@(when number (list :number (yaclml:read-tal-expression-from-string number)))
                                :style ,(if style
                                            (yaclml:read-tal-expression-from-string style)
                                            :default))))

(yaclml::deftag-macro it.bese.ucw.i18n::<format (&attribute date number style)
  `(<:as-html ,(cond
                (date
                 `(cl-icu:write-ustring
                   (cl-icu:uformat (cl-icu:make-date-formatter :locale (cl-icu:make-locale (context.locale *context*))
                                                               :style ,style)
                                   (cl-icu::uobject ,date))
                   *yaclml-stream*))
                (number
                 `(cl-icu:write-ustring
                   (cl-icu:uformat (cl-icu:make-number-formatter :locale (cl-icu:make-locale (context.locale *context*)))
                                   (cl-icu::uobject ,number))
                   *yaclml-stream*))
                (t (error "Must specify either date or number in a UCW:FORMAT tag.")))))

(def-attribute-handler it.bese.ucw.i18n::date-accessor (tag)
  (let* ((accessor-code (yaclml:read-tal-expression-from-string (getf (cdar tag) 'it.bese.ucw.i18n::date-accessor)))
         (style (and (getf (cdar tag) 'it.bese.ucw.i18n::style)
                     (yaclml:read-tal-expression-from-string (getf (cdar tag) 'it.bese.ucw.i18n::style))))
         (accessor-lambda (let ((v (gensym)))
                            `(let ((submitter-locale (context.locale *context*)))
                               (lambda (,v)
                                 (setf ,accessor-code (cl-icu:uparse (cl-icu:make-date-formatter
                                                                      :locale (cl-icu:make-locale submitter-locale)
                                                                      ,@(when style `(:style ,style)))
                                                                     ,v)))))))
    (remf (cdar tag) 'it.bese.ucw.i18n::date-accessor)
    (remf (cdar tag) 'it.bese.ucw.i18n::style)
    (ecase (caar tag)
      (<::INPUT (ucw-accessor accessor-lambda 
                              (append (car tag) `(<::value (html-escape-ustring 
                                                            (cl-icu:uformat (cl-icu:make-date-formatter
                                                                             :locale (cl-icu:make-locale (context.locale *context*))
                                                                             ,@(when style `(:style ,style)))
                                                                            ,accessor-code))))
                              (cdr tag)))
      (<::TEXTAREA (ucw-accessor accessor-lambda
                                 (car tag)
                                 (list `((tal::tal tal::content (html-escape-ustring
                                                                 (cl-icu:uformat (cl-icu:make-date-formatter
                                                                                  :locale (cl-icu:make-locale (context.locale *context*))
                                                                                  ,@(when style `(:style ,style)))
                                                                                 ,accessor-code))))))))))
  
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;; Copyright (c) 2003-2005 Edward Marco Baringer
;;; All rights reserved. 
;;; 
;;; Redistribution and use in source and binary forms, with or without
;;; modification, are permitted provided that the following conditions are
;;; met:
;;; 
;;;  - Redistributions of source code must retain the above copyright
;;;    notice, this list of conditions and the following disclaimer.
;;; 
;;;  - Redistributions in binary form must reproduce the above copyright
;;;    notice, this list of conditions and the following disclaimer in the
;;;    documentation and/or other materials provided with the distribution.
;;; 
;;;  - Neither the name of Edward Marco Baringer, nor BESE, nor the names
;;;    of its contributors may be used to endorse or promote products
;;;    derived from this software without specific prior written permission.
;;; 
;;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
;;; A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT
;;; OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;;; SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
;;; LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
;;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
;;; THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
;;; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
;;; OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
