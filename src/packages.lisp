;;; -*- lisp -*-

;;;; * The Packages

(defpackage :it.bese.ucw
  (:nicknames :ucw)
  (:use :common-lisp
        :it.bese.arnesi
        :it.bese.yaclml
        :iterate
        :trivial-sockets)
  (:export
   ;; backend classes
   #:mod-lisp-backend
   #:multithread-mod-lisp-backend
   #:aserve-backend
   #:araneida-backend
   #:httpd-backend
   #:multithread-httpd-backend
   ;; random configuration options
   #:*inspect-components*
   #:external-format-for
   ;; rerl protocol
   #:*default-server*
   #:standard-server
   #:startup-server
   #:shutdown-server
   #:restart-server
   #:server.backend
   #:server.applications
   #:debug-on-error
   #:*debug-on-error*

   #:*context*
   #:context.window-component
   #:with-dummy-context
   
   #:startup-application
   #:shutdown-application
   #:restart-application
   #:register-application
   #:unregister-application
   #:*default-application*
   #:standard-application
   #:cookie-session-application

   ;; accessing the request/response objects
   #:mime-part-p
   #:mime-part-headers
   #:mime-part-body
   
   #:request
   #:response
   #:html-stream
   ;; backtracking
   #:backtrack
   #:backtrack-slot
   ;; components
   #:defcomponent
   #:compute-url
   #:update-url
   #:standard-component-class
   #:component
   #:parent
   #:widget-component
   #:standard-component
   #:template-component
   #:template-component-environment
   #:simple-template-component
   #:show
   #:show-window
   #:generic-widget-component
   #:widget-component
   #:inline-widget-component

   ;; windows
   #:window-component
   #:simple-window-component
   #:window-component.stylesheet
   #:window-component.javascript
   #:window-component.title
   
   ;; generic componet actions
   #:refresh-component
   #:ok
   
   ;; error message component
   #:error-message
   #:error-component
   
   ;; login component
   #:login
   #:login.username
   #:login.password
   #:try-login
   #:check-credentials
   #:login-successful

   ;; info-message component
   #:info-message

   ;; option dialog component
   #:option-dialog
   #:respond

   ;; container component
   #:container
   #:simple-container
   #:list-container
   #:component-at
   #:add-component
   #:container.current-component-name
   #:container.current-component
   #:container.label-test
   #:container.contents
   #:switch-component
   #:find-component
   #:initialize-container

   ;; inspector
   #:ucw-inspector
   #:inspect-anchor
   
   ;; forms
   #:accept
   #:present
   #:ieview
   #:view
   #:label
   #:label-plural
   #:editablep
   #:labelp
   #:presentp
   #:text
   #:decimal
   #:interface-element
   #:form-element
   #:auxslots-form-element
   #:integer-element
   #:slot-elements
   #:number-element
   #:text-element
   #:password-element
   #:checkbox-element
   #:date-element
   #:text-area-element
   #:select-element
   #:decimal-element
   #:time-element
   #:integer-range-element
   #:time-element
   #:read-client-value
   #:format-lisp-value
   #:validate-value
   #:default-value
   #:client-value
   #:lisp-value
   #:filter-client-value
   #:direct-value
   #:coerce-client-value
   #:submit
   #:completing-text-element
   #:completions
   #:completer.js-path
   #:completer-class
   #:validp
   #:indirect-value
   #:slot-indirect-makunbound 
   #:slot-indirect-value 
   #:slot-indirect-p
   #:slot-indirect-object
   #:set-element
   #:item-viewer
   #:items
   #:rownum
   #:render-item
   #:render-items
   #:render-slots
   #:render-slot
   #:render-slot-labels
   #:range-set-element
   #:range-set
   #:add-item
   #:delete-item
   #:composite-element
   #:composite
   #:search-element
   #:query-value

   ;; interface-elements
   #:ie-constraint-violation
   #:ie-bad-input-type
   #:ie-bad-input-format
   #:define-ie-constraint-method
   #:signal-ie-constraint-violation
   #:ie-constraint
   #:define-ie-type
   #:ie-type-class
   #:ie-type
   #:ie-type-precedence-list
   #:ie-type-attributes
   #:ie-type-of
   #:ie-condition
   #:ie-bad-input
   #:define-ie-constraint-generic
   #:define-ie-generic
   #:define-ie-method
   #:define-ie-action

   ;; forms
   #:form-field
   #:generic-html-input
   #:validators
   #:validp
   #:value
   #:dom-id
   
   #:submit-button

   #:string-field
   #:textarea-field
   #:integer-field
   #:select-field
   #:password-field
   #:checkbox-field
   #:file-upload-field
   #:alist-select-field
   #:hash-table-select-field
   #:plist-select-field

   #:in-field-password-field
   #:in-field-string-field
   
   #:date-field
   #:date-ymd
   #:date-in-range-validator
   #:dmy-date-field
   #:mdy-date-field
   
   
   #:length-validator
   #:not-empty-validator
   #:string=-validator
   #:integer-range-validator
   #:e-mail-address-validator
   
   #:simple-form
   
   ;; range-view component
   #:range-view
   #:render-range-view-item
   #:range-view.current-window
   #:range-view.current-window-items
   #:range-view.windows
   #:range-view.have-next-p
   #:range-view.have-previous-p
   
   ;; the date picker component
   #:generic-date-picker
   #:dropdown-date-picker
   #:date-picker.year
   #:date-picker.day
   #:date-picker.month
   #:date-picker.partial-date-p
   #:date-picker.complete-date-p

   #:redirect-component
   #:redirect
   
   ;; the tabbed-pane component
   #:tabbed-pane

   ;; the task component
   #:task-component
   #:start

   ;; status bar component
   #:status-bar
   #:add-message
   #:show-message

   ;; cache
   #:cached-component
   #:cached-output
   #:timeout
   #:component-dirty-p
   #:refresh-component-output
   #:timeout-cache-component
   #:num-hits-cache-component
   
   ;; transactions
   #:transaction-mixin
   #:open-transaction
   #:close-transaction

   ;; secure application
   #:secure-application-mixin
   #:secure-application-p
   #:application-find-user
   #:application-check-password
   #:application-authorize-call
   #:on-authorization-reject
   #:session-user
   #:session-authenticated-p
   #:user-login
   #:login-user
   #:logout-user
   #:exit-user
   
   ;; actions
   #:defaction
   #:defentry-point
   #:standard-entry-point
   #:regexp-entry-point
   #:self
   #:call
   #:call-component
   #:call-as-window
   #:answer
   #:answer-component
   #:jump
   #:jump-to-component
   #:component.place
   #:make-place
   #:action-href
   #:action-href-body

   ;; session
   #:get-session-value
   
   #:register-action
   #:register-callback
   ;; yaclml/tal
   #:*ucw-tal-root*
   #:render

   ;; publishing files, directories and other "stuff"
   #:publish-directory

   ;; Control utilities
   #:start-swank
   #:create-server
   #:hello
   #:bye-bye))

(defpackage :it.bese.ucw-user
  (:use :common-lisp
        :it.bese.ucw
        :it.bese.arnesi
        :it.bese.yaclml))

(defpackage :it.bese.ucw.tags
  (:documentation "UCW convience tags.")
  (:use)
  (:nicknames #:<ucw)
  (:export
   #:render-component
   #:a
   #:area
   #:form
   #:input
   #:button
   #:select
   #:option
   #:textarea

   #:integer-range-select
   #:month-day-select
   #:month-select

   #:text
   #:password
   #:submit

   #:script))

;;;;@include "rerl/protocol.lisp"

;;;; * Components

;;;;@include "components/login.lisp"

;;;;@include "components/error.lisp"

;;;;@include "components/message.lisp"

;;;;@include "components/option-dialog.lisp"

;;;;@include "components/range-view.lisp"

;;;;@include "components/redirect.lisp"

;;;;@include "components/tabbed-pane.lisp"

;;;;@include "components/task.lisp"

;;;;@include "components/ucw-inspector.lisp"

;;;; * Meta Components

;;;;@include "components/widget.lisp"

;;;;@include "components/window.lisp"

;;;;@include "components/template.lisp"

;;;;@include "components/container.lisp"

;;;; * Standard RERL Implementation

;;;;@include "rerl/standard-server.lisp"

;;;;@include "rerl/standard-application.lisp"

;;;;@include "rerl/standard-session.lisp"

;;;;@include "rerl/cookie-session.lisp"

;;;;@include "rerl/standard-session-frame.lisp"

;;;;@include "rerl/standard-action.lisp"

;;;;@include "rerl/backtracking.lisp"

;;;;@include "rerl/request-loop-error.lisp"

;;;;@include "rerl/conditions.lisp"

;;;;@include "rerl/standard-vars.lisp"

;;;; ** Standard Component

;;;; * The Backends

;;;;@include "backend/httpd.lisp"

;; Copyright (c) 2003-2005 Edward Marco Baringer
;; All rights reserved. 
;; 
;; Redistribution and use in source and binary forms, with or without
;; modification, are permitted provided that the following conditions are
;; met:
;; 
;;  - Redistributions of source code must retain the above copyright
;;    notice, this list of conditions and the following disclaimer.
;; 
;;  - Redistributions in binary form must reproduce the above copyright
;;    notice, this list of conditions and the following disclaimer in the
;;    documentation and/or other materials provided with the distribution.
;; 
;;  - Neither the name of Edward Marco Baringer, nor BESE, nor the names
;;    of its contributors may be used to endorse or promote products
;;    derived from this software without specific prior written permission.
;; 
;; THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
;; "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
;; LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
;; A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT
;; OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
;; SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
;; LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
;; DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
;; THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
;; (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
;; OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
